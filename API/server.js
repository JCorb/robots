const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//inicialitzem mongoose
const db = require("mongoose");
db.Promise = global.Promise;

const conn_url = "mongodb://localhost:27017/robots_db";

//connectem a la bdd
db.connect(conn_url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("Conectado a db!");
    })
    .catch(err => {
        console.log("Conexión  falla...", err);
        process.exit();
    });

db.set('useFindAndModify', false);

//creem esquema i model 'Contacte'
const robotCollection = db.Schema(
    {nom : String, 
      caracteristica: {
          fuerza: {type : Number, min:1, max:5},
          habilidad: {type : Number, min:1,max:5},
          velocidad:  {type :Number, min:1, max:5},
          
    },
    experiencia : {
        batallas_ganadas : Number,
        batallas_perdidas: Number,
    }
});

const batallasCollection = db.Schema({
    id_ganador : Number,
    id_perdedor: Number,
})



const robot = db.model('robots', robotCollection);
const batallas = db.model('batallas', batallasCollection);



// Lista de todos los robots
app.get("/", (req, res) => {
    robot.find()
    .then(data => res.json(data))
});

// crea nuevo robot
app.post("/api/robots", (req, res) => {
    robot.create(req.body)
    .then(data => res.json(data))
    .catch(err => res.json({err :err}))
});


// robot por ID
app.get("/api/robots/:id", (req, res) => {
    const id = req.params.id;
    robot.findById(id)
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "ERROR"
            });
        });
});

// Trabajando en ello
app.get("/api/batalla/:id1/:id2", (req,res) => {
    if(!req.params){
        res.json({params : "Añade ids de los robots para empezar la batallas"})
    }

        const {id1}= req.params;
        const {id2}= req.params;

        robot.find({"_id" : {
            $in : [
            db.Types.ObjectId(id1),
            db.Types.ObjectId(id2)
            ]
        }})
        .then(robots => {
            const robot1 = robots[0];
            const robot2 = robots[1];

            const potencia1 = robot1.fuerza*robot1.velocidad+robot1.habilidad;
            const potencia2 = robot2.fuerza*robot2.velocidad+robot2.habilidad;

            if (potencia1>potencia2){
                res.status(200).json(robot1);

            } else {
                res.status(200).json(robot2);
            }



            // const sumaRobots = robots.map(value => value.caracteristica.fuerza += (value.caracteristica.velocidad+value.caracteristica.habilidad))

           
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "ERROR"
            });
        });


       
})



// set port, listen for requests
const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
