import React, { useContext, useEffect, useState } from "react";
import Controller from "./ContactesController";
import { Row, Col,Table } from "reactstrap";
//import TraductorContext from "./TraductorContext.js";

export default () => {
    const [dades, setDades] = useState([]);
    const [error, setError] = useState('');
  
//    const Traductor = useContext(TraductorContext);
    useEffect(() => {
      Controller.getAll()
        .then(data => setDades(data.sort((a,b) => {
          return(a.experiencia.batallas_ganadas<b.experiencia.batallas_ganadas ? 1:-1)
        })))
        .catch(err => setError(err.message));
    }, [])     
    
    const filas = dades.map((el) => (
      <tr key={el.id}>
        <td>{el.nom}</td>
        <td>{el.experiencia.batallas_ganadas}</td>
        <td>{el.experiencia.batallas_perdidas}</td>
      </tr>
    ))

    return (
      <>
        <Row>
        <Table striped>
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Batallas ganadas</th>
          <th>Batallas perdidas</th>
        </tr>

      
      </thead>
      <tbody>{filas}</tbody>
      <tfoot></tfoot>
    </Table>
        </Row>
  
  
        {error && <h3>Error: {error}</h3>}
      </>
    );
  };
  