import React, { useContext, useEffect, useState } from "react";
import Controller from "./ContactesController";
import { Row, Col, Input,Button } from "reactstrap";
import TraductorContext from "./TraductorContext.js";
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import Taula from "./Taula";

export default () => {
  const [dades, setDades] = useState([]);
  const [error, setError] = useState('');
  const [robot1, setRobot1] = useState();
  const [robot2, setRobot2] = useState();
  const [robotData, setrobotData] = useState(null);
  const [open,setOpen] = useState(false);




  

  const Traductor = useContext(TraductorContext);


  useEffect(() => {
    Controller.getAll()
      .then(data => {
        setDades(data);
        setRobot1(data[0].nom);
        setRobot2(data[1].nom);
      })
      .catch(err => setError(err.message));
  }, [])


  // const cargaDatos = async () => {
  //   const data = await Controller.getAll();
  //   setDades(data);
  // }

  // useEffect(() => {
  //  cargaDatos();
  // }, []);

  // useEffect(() => {
  //   Controller.getByIds("5fb3f2f11b1b9a28c88f2f20","5fb3f5fff848062a58ad94d0")
  //   .then(data =>setrobotData(data))
  //   .catch(err => console.log(err))
  // },[]);


  
  const StartLucha = () => {
    const id1 = dades.find(value => value.nom === robot1);
    const id2 = dades.find(value => value.nom === robot2);
    Controller.getByIds(id1.id,id2.id)
    .then(data => {
      console.log("xxx", data);
      setrobotData(data);
    })
    .catch(err => console.log(err))

    setOpen(!open);
  }
  

  const options = dades.map(value => {
    return (
      <option>{value.nom}</option>
    )
  });



  return (
    <>
      <Row>
        <Col>
          
        </Col>
      </Row>
      <Row>
        <Col xs="4">
          <Input type="select" name="select" id="exampleSelect" value={robot1} onChange={(e) => setRobot1(e.target.value)}>
            {options}
          </Input>

          <Taula datos={dades} robot={robot1}/>

        </Col>
        <Col xs ="4" >
          {/* <img src='/img/vs.png' className="img-fluid"/>*/}
        </Col>

      
        <Col xs="4">
          <Input type="select" name="select" id="robot" value={robot2} onChange={(e) => setRobot2(e.target.value)}>
           {options}
          </Input>
          <Taula datos={dades} robot={robot2}/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Button color="danger" size="lg" onClick={StartLucha}>Lucha</Button>
        </Col>
      </Row>

     {/*Modal GANADOR */ }
      <div>
      <Button color="danger" onClick={StartLucha}></Button>
      <Modal isOpen={open} toggle={StartLucha}>
        <ModalHeader toggle={StartLucha}>GANADOR</ModalHeader>
        <ModalBody>
          <h1>{robotData && robotData.nom}!!</h1>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={StartLucha}>Do Something</Button>{' '}
          <Button color="secondary" onClick={StartLucha}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </div>

      {error && <h3>Error: {error}</h3>}
    </>
  );
};
