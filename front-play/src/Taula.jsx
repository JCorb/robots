import React from "react";

import { Card, CardImg, CardBody, CardTitle, } from "reactstrap";

// importamos TraductorContext
//import TraductorContext from "./TraductorContext.js";

export default (props) => {
  const robot = props.datos.find(value => value.nom === props.robot);

  // useContext da acceso al contenido del último provider para este contexto
  // en este caso, el contenido incluye la función traduce, que utilizamos para 
  // mostrar los mensajes en el idioma de la aplicación
  // al cambiar el idioma se produce un nuevo render en App que afecta a todo el arbol de componentes
  //const Traductor = useContext(TraductorContext);

  return (
    <>
      <div>
        <Card>
          <CardImg top width="90%" src={`https://robohash.org/${props.robot}`} alt="Card image cap" />
          <CardBody>
            <CardTitle tag="h3">{props.robot}</CardTitle>
            {robot &&
              <>
                <h6> {"Fuerza " + robot.caracteristica.fuerza}</h6>
                <h6> {"Habilidad " + robot.caracteristica.habilidad}</h6>
                <h6> {"Velocidad " + robot.caracteristica.velocidad}</h6>
              </>
            }

          </CardBody>
        </Card>
      </div>
    </>
  );
};
