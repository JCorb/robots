import React, { useContext, useEffect, useState } from "react";
import Controller from "./ContactesController";
import { Link } from "react-router-dom";

import { Row, Col } from "reactstrap";
import TraductorContext from "./TraductorContext.js";

import Taula from "./Taula";

export default () => {
  const [dades, setDades] = useState([]);
  const [error, setError] = useState('');

  const Traductor = useContext(TraductorContext);
  useEffect(()=>{
    Controller.getAll()
     .then(data => setDades(data))
     .catch(err => setError(err.message));
   }, [])


  // const cargaDatos = async () => {
  //   const data = await Controller.getAll();
  //   setDades(data);
  // }

  // useEffect(() => {
  //  cargaDatos();
  // }, []);

 

  return (
    <>
      <Row>
        <Col>
          <h3>{Traductor.traduce("listado")}</h3>
        </Col>
        <Col className="text-right">
          <Link className="btn btn-success btn-sm" to="/contactos/nuevo">
            {Traductor.traduce("nuevo")}
          </Link>
        </Col>
      </Row>

      <Taula
        datos={dades}
        rutaShow="/contactos/detalle/"
        rutaEdit="/contactos/editar/"
        rutaDelete="/contactos/eliminar/"
      />

      {error && <h3>Error: {error}</h3>}
    </>
  );
};
