import React, {useContext, useEffect} from "react";
import { Table } from "reactstrap";
import {
  Link,
} from "react-router-dom";

import Controller from './ContactesController';

// importamos TraductorContext
import TraductorContext from "./TraductorContext.js";

export default (props) => {

  

  const filas = props.datos.map((el) => (
    <tr key={el.id}>
      <td>{el.nom}</td>
      <td>{el.caracteristica.fuerza}</td>
      <td>{el.caracteristica.habilidad}</td>
      <td>{el.caracteristica.velocidad}</td>
      <td> <img width= "100px" src={`https://robohash.org/${el.nom}`} alt=""/>  </td>
    </tr>
  ));


  useEffect(()=>{
    fetch(`https://robohash.org/`)
  },[])

  return (
    <>
    <Table dark>
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Fuerza</th>
          <th>Habilidad</th>
          <th>Velocidad</th>
          <th>Foto</th>
        </tr>
        </thead>
      <tbody>{filas}</tbody>
      <tfoot></tfoot>
    </Table>
    </>
  );
};
