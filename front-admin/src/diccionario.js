
const diccionario = {
    titol: ['Mi listado de robots', 'El meu llistat de robots'],
    inici: ['Inicio', 'Casa'],
    contactos: ['Robots', 'Robots'],
    listado: ['Listado de robots', 'Llistat de robots'],
    nombre: ['Nombre', 'Nom'],
    email: ['Características', 'Caracteristicas'],
    mostrar: ['Mostrar', 'Mira'],
    editar: ['Editar', 'Modifica'],
    eliminar: ['Eliminar', 'Esborra'],
    nuevo: ['Crear robot', 'Crear robot'],
    volver: ['Volver', 'Torna'],
    tel: ['Habilidades', 'Habilitats'],
    cancelar: ['Cancelar', 'Deixa-ho estar'],
    guardar: ['Guardar', 'Desa els canvis'],
    detalle: ['Detalle', 'Detall']

}

export default diccionario;