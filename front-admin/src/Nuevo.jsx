import React, {useState, useEffect} from "react";
import {Redirect, Link} from "react-router-dom";
import {FormGroup, Label, Input, Alert} from 'reactstrap';

import Controller from './ContactesController';

export default (props) => {

  const [nom, setNom] = useState('');
  const [fuerza, setFuerza] = useState(1);
  const [habilidad, setHabilidad] = useState(1);
  const [velocidad, setVelocidad] = useState(1);

  const [volver, setVolver] = useState();

  let RobotNuevo = {
    nom,
    caracteristica: { 
    fuerza,
    habilidad,
    velocidad
    }};

  const guardar = () => {

    if(fuerza*1 + habilidad*1 + velocidad*1 < 10){ 
      alert("¡ok!")
      let RobotNuevo = {
        nom,
        caracteristica: { 
        fuerza,
        habilidad,
        velocidad
        },
        experiencia : {
          batallas_ganadas : 0,
          batallas_perdidas: 0
      }
      }; 
      Controller.addItem(RobotNuevo);
    }else (alert("¡Entre todos no pueden sumar más de 10!"));{
    setVolver(false);
    };
    

    setVolver(true);
  }


  if (volver){
    return <Redirect to="/contactos" />
  }

  return (
    <>
      <h3>Editar</h3>
      <hr />

      <FormGroup>
        <Label for="nom">Nombre</Label>
        <Input type="text" name="nom" id="nom" value={nom} onChange={(e) => setNom(e.target.value)} />
      </FormGroup>

      <FormGroup>
  <Label for="exampleSelect">Selecciona fuerza</Label>
        <Input type="select" name="select" id="exampleSelect"
        value={fuerza} onChange={(e) => setFuerza(e.target.value)}>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </Input>
      </FormGroup>

      <FormGroup>
        <Label for="exampleSelect">Selecciona habilidad</Label>
        <Input type="select" name="select" id="exampleSelect"
        value={habilidad} onChange={(e) => setHabilidad(e.target.value)}>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </Input>
      </FormGroup>

      <FormGroup>
        <Label for="exampleSelect">Selecciona velocidad</Label>
        <Input type="select" name="select" id="exampleSelect"
        value={velocidad} onChange={(e) => setVelocidad(e.target.value)}>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </Input>
      </FormGroup>

      <hr />
      <Link className='btn btn-primary' to='/contactos' >Volver</Link>
      {' '}
      <button className='btn btn-success' onClick={guardar} >Guardar</button>
    </>
  );
};
